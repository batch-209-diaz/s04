const chai = require('chai');
const {assert} = require('chai');

const http = require('chai-http');
chai.use(http);

describe("api_test_suite_register", () => {

	it('test_api_post_register_returns_400_if_no_name', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/register')
		.type('json')
		.send({
		    username: "iamjson12",
	      	age: 28
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();
		})
	})

	it("test_api_post_register_is_running", () => {
		chai.request('http://localhost:4000')
		.post('/users/register')
		.type('json')
		.send({
			username: "iamjson12",
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			assert.notEqual(res.status,404);
			done();
		})
	})

	it('test_api_post_register_returns_400_if_no_username', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/register')
		.type('json')
		.send({
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/register')
		.type('json')
		.send({
			username: "iamjson12",
			name: "Jay White",
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

})

describe("api_test_suite_login",()=>{

	it('test_api_post_login_returns_400_if_no_username', (done) => {

		chai.request('http://localhost:4000')
		.post('/users/login')
		.type('json')
		.send({
			password: "87brandon19"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_login_returns_400_if_no_password', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/login')
		.type('json')
		.send({
			username: "brBoyd87"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_login_returns_200_if_correct_credentials', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon19"
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})

    it('test_api_post_login_returns_403_if_incorrect_password', (done) => {
		chai.request('http://localhost:4000')
		.post('/users/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "wrongpassword"
		})
		.end((err, res) => {
			assert.equal(res.status,403)
			done();
		})
	})


})
