const { assert } = require('chai');
const { getCircleArea } = require("../src/util.js")
const { getNumberOfChar } = require("../src/util.js")

describe('test_get_circle_area',()=>{

    it('test_area_of_circle-radius_15_is_706.86',()=>{

        let area = getCircleArea(15);
        assert.equal(area,706.86);
    })

    it('test_area_of_circle-negative_radius_is_undefined',()=>{

        let area = getCircleArea(-1);
        assert.isUndefined(area);
    })

    it('test_area_of_circle-zero_radius_is_undefined',()=>{

        let area = getCircleArea(0);
        assert.isUndefined(area);
    })

    it('test_area_of_circle-false_radius_is_undefined',()=>{

        let area = getCircleArea(false);
        assert.isUndefined(area);
    })

})

describe('test_get_number_of_char_in_sentence',()=>{

    it('test_number_of_l_in_string_is_3',()=>{

        let numChar = getNumberOfChar("l","Hello World!");
        assert.equal(numChar,3);
    })

    it('test_number_of_a_in_string_is_2',()=>{

        let numChar = getNumberOfChar("a","Malta");
        assert.equal(numChar,2);
    })

    it('test_undefined_if_first_arg_not_string',()=>{

        let numChar = getNumberOfChar(true,"true");
        assert.isUndefined(numChar);
    })

    it('test_undefined_if_second_arg_not_string',()=>{

        let numChar = getNumberOfChar("f",false);
        assert.isUndefined(numChar);
    })

})