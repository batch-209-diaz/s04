const express = require('express');
const app = express();
const PORT = 4000;

app.use(express.json());

let userRoutes = require('./routes/routes');
app.use('/users', userRoutes);

app.listen(PORT, ()=>{

    console.log('Running on port ' + PORT)

})











/*
    Test Driven Development

    TDD is not just tests per se - it is about the underlying design and development processes:

    - Consider how components mesh together
    - Consider how efficiently one part of your codes work with the other codes
    - Ex. the habit of adding console logs and forgetting to edit them out

    TDD Process:

    1. Write a test - fails
    2. Edit to Pass Code
    3. Refactor - improve, make clean

*/