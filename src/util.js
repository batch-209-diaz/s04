function getCircleArea(radius){

    // if(radius <= 0 || typeof(radius) != 'number'){
    //     return undefined
    // }

    if(radius <= 0 || isNaN(radius)){
        return undefined
    }

    return 3.1416*(radius**2);

}

function getNumberOfChar(char,string){
    let counter = 0;
    if(typeof(char) != 'string' || typeof(string) != 'string'){
        return undefined
    } else {
        let characters = string.split("");
        characters.forEach(character =>{
            if(character === char){
                counter++;
            }
        })
    }
    return counter;
}

let users = [

    {
        username: "brBoyd87",
        password: "87brandon19"
    },
    {
        username: "tylerOfsteve",
        password: "stevenstyle75"
    }
]

module.exports = {
    getCircleArea,
    getNumberOfChar,
    users
}